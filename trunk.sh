#!/bin/bash
ALLTRUNK=`/usr/sbin/asterisk -rx "sip show registry"`
TRUNKCOUNT=`echo "$ALLTRUNK" | grep "SIP registrations" | awk '{print $1}'`
REGTRUNK=`echo "$ALLTRUNK" | grep "Registered" | wc -l`

if [ $REGTRUNK -lt $TRUNKCOUNT ]
then
    echo `/usr/sbin/asterisk -rx "sip reload"`
else
    echo "ALL TRUNKS STATUS OK" >> /var/log/asterisk/trunkCheck.log
fi
