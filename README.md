## Reload SIP Trunk

#### Description
The script restarts the process of raising trunks if one of them is not in the registered state.

#### Install

1. Copy file on you FreePBX server.

2. Give executeble right on this file

3. Add cron rules

4. Check log-files /var/log/asterisk/trunkCheck.log